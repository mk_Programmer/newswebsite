﻿using NewsWebsite.Entities.Identity;
using System.Threading.Tasks;

namespace NewsWebsite.Services.Api.Contract
{
    public interface IJwtService
    {
        Task<string> GenerateTokenAsync(User User);
    }
}
