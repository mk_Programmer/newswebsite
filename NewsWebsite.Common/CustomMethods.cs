﻿using System;

namespace NewsWebsite.Common
{
    public static class CustomMethods
    {
        public static int RandomNumber(int min, int max) // تولید عدد تصادفی در بازه
        {
            Random random = new Random();
            return random.Next(min, max); // اعدادی را برمی گرداند که بزرگتر و مساوی مین ولی کوجکتر از مکس هستند Next() متد
        } 
    }
}
