﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NewsWebsite.IocConfig.Extensions;
using NewsWebsite.Services.Contracts;
using NewsWebsite.Services.Identity;

namespace NewsWebsite.IocConfig
{
    public static class IdentityServicesRegistry
    {
        public static void AddCustomIdentityServices(this IServiceCollection services)
        {
            services.AddIdentityOptions();
            services.AddDynamicPersmission();
            services.AddScoped<IApplicationUserManager, ApplicationUserManager>();
            services.AddScoped<IApplicationRoleManager, ApplicationRoleManager>();
            services.AddScoped<IIdentityDbInitializer, IdentityDbInitializer>(); // به برنامه IdentityDbInitializer() سرویسی برای بزریق یک نمونه از کلاس
            services.AddScoped<ApplicationIdentityErrorDescriber>();
        }


        public static void UseCustomIdentityServices(this IApplicationBuilder app)
        {
            // set Information admin user for when run the Project
            app.UseAuthentication();
            app.CallDbInitializer();
        }


        private static void CallDbInitializer(this IApplicationBuilder app)
        {
            var scopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();

            using (var scope = scopeFactory.CreateScope())
            {
                var identityDbInitialize = scope.ServiceProvider.GetService<IIdentityDbInitializer>();
                identityDbInitialize.Initialize();
                identityDbInitialize.SeedData();
            }
        }
    }
}
