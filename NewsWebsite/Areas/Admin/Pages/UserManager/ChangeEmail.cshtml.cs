using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NewsWebsite.Data.Contracts;
using NewsWebsite.ViewModels.UserManager;

namespace NewsWebsite.Areas.Admin.Pages.UserManager
{
    public class ChangeEmail : PageModel
    {
        private readonly IUnitOfWork _uw;
        public ChangeEmail(IUnitOfWork uw)
        {
            _uw = uw;
        }


        public PageResult OnGet()
        {
            return Page();
        }


        //public async Task<IActionResult> OnPostAsync()
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return Page();
        //    }

        //    await _uw.BaseRepository<UsersViewModel>().CreateAsync(Users);
        //    await _uw.Commit();

        //    return RedirectToPage("./Index");
        //}
    }
}
