#pragma checksum "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fef32e53711da63fce792266b73680e72e17c88c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_VideoDetails), @"mvc.1.0.view", @"/Views/Home/VideoDetails.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 2 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
using NewsWebsite.Common.Extensions;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fef32e53711da63fce792266b73680e72e17c88c", @"/Views/Home/VideoDetails.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"263f5f7032912694fc818ebb6bd514c8d5a64492", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_VideoDetails : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<NewsWebsite.Entities.Video>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 4 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
  
    ViewData["Title"] = "VideoDetails";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div class=""vizew-breadcrumb"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-12"">
                <nav aria-label=""breadcrumb"">
                    <ol class=""breadcrumb"">
                        <li class=""breadcrumb-item""><a href=""/""><i class=""fa fa-home"" aria-hidden=""true""></i>خانه</a></li>
                        <li class=""breadcrumb-item""><a href=""/Videos"">ویدیوها</a></li>
                        <li class=""breadcrumb-item active"" aria-current=""page"">");
#nullable restore
#line 17 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
                                                                          Write(Model.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<section class=""post-details-area mb-80"">
    <div class=""container"">
        <div class=""row"">
            <div class=""col-12"">
                <div class=""single-video-area"">
                    <div class=""post-thumbnail"">
                        <video width=""100%""");
            BeginWriteAttribute("poster", " poster=\"", 1080, "\"", 1116, 1);
#nullable restore
#line 31 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
WriteAttributeValue("", 1089, "/posters/"+Model.Poster, 1089, 27, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" controls><source");
            BeginWriteAttribute("src", " src=\"", 1134, "\"", 1150, 1);
#nullable restore
#line 31 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
WriteAttributeValue("", 1140, Model.Url, 1140, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Your browser does not support the video tag.</video>\r\n                        <span class=\"video-duration\">");
#nullable restore
#line 32 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
                                                Write(Model.PublishDateTime.ConvertMiladiToShamsi("dd MMMM yyyy ساعت HH:mm"));

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n                    </div>\r\n                    <div class=\"post-content\">\r\n                        <p style=\"font-size:19px;\">");
#nullable restore
#line 35 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Home\VideoDetails.cshtml"
                                              Write(Model.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<NewsWebsite.Entities.Video> Html { get; private set; }
    }
}
#pragma warning restore 1591
