#pragma checksum "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cb97dfe5e4d121b596fb70176bb41272fa2afc88"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared_Components_LatestNewsTitleList_Default), @"mvc.1.0.view", @"/Views/Shared/Components/LatestNewsTitleList/Default.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cb97dfe5e4d121b596fb70176bb41272fa2afc88", @"/Views/Shared/Components/LatestNewsTitleList/Default.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"263f5f7032912694fc818ebb6bd514c8d5a64492", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared_Components_LatestNewsTitleList_Default : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<NewsWebsite.ViewModels.News.NewsViewModel>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<div class=\"breaking-news-area d-flex align-items-center\">\r\n    <div class=\"news-title\">\r\n        <p>تیتر جدیدترین اخبار :</p>\r\n    </div>\r\n    <div id=\"breakingNewsTicker\" class=\"ticker\">\r\n        <ul>\r\n");
#nullable restore
#line 9 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml"
             foreach (var item in Model)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <li><a");
            BeginWriteAttribute("href", " href=\"", 341, "\"", 376, 4);
            WriteAttributeValue("", 348, "/News/", 348, 6, true);
#nullable restore
#line 11 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml"
WriteAttributeValue("", 354, item.NewsId, 354, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 366, "/", 366, 1, true);
#nullable restore
#line 11 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml"
WriteAttributeValue("", 367, item.Url, 367, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 11 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml"
                                                      Write(item.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a></li>\r\n");
#nullable restore
#line 12 "D:\S T A R\Training\Asp.net\Asp.net Core\NewsWebsite\NewsWebsite\Views\Shared\Components\LatestNewsTitleList\Default.cshtml"
            }

#line default
#line hidden
#nullable disable
            WriteLiteral("        </ul>\r\n    </div>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<NewsWebsite.ViewModels.News.NewsViewModel>> Html { get; private set; }
    }
}
#pragma warning restore 1591
