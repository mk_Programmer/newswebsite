﻿using NewsWebsite.Common.Extensions;
using System;
using Xunit;

namespace NewsWebsite.XUnitTest.Common
{
    public class ObjectExtensionsTest
    {
        [Fact]
        public void CheckArgumentIsNullTest()
        {
            Assert.Throws<ArgumentNullException>(() => ObjectExtensions.CheckArgumentIsNull(null, "_keyNormalizer"));
        }
    }
}
