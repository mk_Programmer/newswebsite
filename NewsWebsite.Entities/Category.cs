﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewsWebsite.Entities
{
    public class Category
    {
        [Key]
        public string CategoryId { get; set; }
        public string CategoryName { get; set; }

        [ForeignKey("Parent")]
        public string ParentCategoryId { get; set; }
        public string Url { get; set; }

        public virtual Category Parent { get; set; } // را که در ویو مدل هست مقداددهی کرد ParentCategoryName این نویگیشن پراپرتی اطلاعات دسته پدر را هم در خود قرار می دهد و میتوان پراپرتی
        public virtual List<Category> Categories { get; set; }

        public ICollection<NewsCategory> NewsCategories { get; set; }

    }
}
