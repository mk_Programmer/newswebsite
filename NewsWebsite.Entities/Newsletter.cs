﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NewsWebsite.Entities
{
    public class Newsletter
    {
        public Newsletter() { }
        public Newsletter(string email)
        {
            Email = email;
        }


        [Key]
        public string Email { get; set; }
        public DateTime? RegisterDateTime { get; set; } // هنگام عضو شدن کاربر در خبرنامه این پراپرتی خودکار مقدار می گیرد
        public bool IsActive { get; set; } // مثل پراپرتی بالا خودکار مقدار می گیرد
    }
}
