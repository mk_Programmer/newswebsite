﻿using NewsWebsite.Entities.Identity;

namespace NewsWebsite.Entities
{
    public class Bookmark
    {
        public string NewsId { get; set; }
        public int UserId { get; set; } // این پراپرتی گلید خارجی و رابط بین این کلاس و انتیتی یوزر است

        public virtual News News { get; set; }
        public virtual User User { get; set; }
    }
}
