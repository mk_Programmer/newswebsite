﻿using System;
using System.ComponentModel.DataAnnotations;

namespace NewsWebsite.Entities
{
    public class Video
    {
        [Key]
        public string VideoId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; } // آدرس هاستی که ویدئو روی ان است
        public string Poster { get; set; }
        public DateTime? PublishDateTime { get; set; }
    }
}
