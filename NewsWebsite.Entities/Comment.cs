﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NewsWebsite.Entities
{
    public class Comment
    {
        public Comment()
        {
            Comments = new List<Comment>(); // نمایش آبشاری نظرها که مقدار کامنت نال نباشد
        }


        [Key]
        public string CommentId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string NewsId { get; set; }
        public bool IsConfirm { get; set; }
        public DateTime? PostageDateTime { get; set; }

        [ForeignKey("comment")]
        public string ParentCommentId { get; set; }

        public virtual Comment comment { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual News News { get; set; }
    }
}
