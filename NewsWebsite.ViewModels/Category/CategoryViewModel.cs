﻿using NewsWebsite.Common.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace NewsWebsite.ViewModels.Category
{
    public class CategoryViewModel
    {
        [JsonPropertyName("Id")]
        public string CategoryId { get; set; }


        [Display(Name = "عنوان دسته بندی"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("دسته")]
        public string CategoryName { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "آدرس"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("آدرس"), UrlValidate("/", @"\", " ")]
        public string Url { get; set; }


        [JsonIgnore]
        public string ParentCategoryId { get; set; }


        [Display(Name = "دسته پدر"), JsonPropertyName("دسته پدر")]
        public string ParentCategoryName { get; set; }
    }
}
