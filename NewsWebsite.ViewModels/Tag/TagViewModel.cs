﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace NewsWebsite.ViewModels.Tag
{
    public class TagViewModel
    {
        [JsonPropertyName("Id")]
        public string TagId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "عنوان برچسب"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("برچسب")]
        public string TagName { get; set; }
    }
}
