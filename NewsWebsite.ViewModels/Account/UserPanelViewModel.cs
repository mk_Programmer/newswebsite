﻿using NewsWebsite.Entities.Identity;
using NewsWebsite.ViewModels.News;
using System.Collections.Generic;

namespace NewsWebsite.ViewModels.Account
{
    public class UserPanelViewModel
    {
        public UserPanelViewModel(User user, List<NewsViewModel> bookmarks)
        {
            User = user;
            Bookmarks = bookmarks;
        }


        public User User { get; set; }

        public List<NewsViewModel> Bookmarks { get; set; }
    }
}
