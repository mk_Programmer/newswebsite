﻿using NewsWebsite.ViewModels.Category;
using System.Collections.Generic;

namespace NewsWebsite.ViewModels.News
{
    public class NewsCategoriesViewModel
    {
        public NewsCategoriesViewModel(List<TreeViewCategory> categories, string[] categoryIds)
        {
            Categories = categories;
            CategoryIds = categoryIds;
        }


        public string[] CategoryIds { get; set; }

        public List<TreeViewCategory> Categories { get; set; } // اطلاعات زیر دسته ها
    }
}
