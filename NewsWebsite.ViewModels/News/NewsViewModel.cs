﻿using NewsWebsite.Common.Attributes;
using NewsWebsite.Entities;
using NewsWebsite.Entities.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace NewsWebsite.ViewModels.News
{
    public class NewsViewModel
    {
        [JsonPropertyName("Id")]
        public string NewsId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "عنوان خبر"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("عنوان خبر")]
        public string Title { get; set; }


        [JsonPropertyName("ShortTitle")]
        public string ShortTitle { get; set; }


        [JsonIgnore]
        public bool FuturePublish { get; set; }


        [Display(Name = "چکیده"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string Abstract { get; set; }


        [JsonIgnore]
        public DateTime? PublishDateTime { get; set; }


        [Display(Name = "تاریخ انتشار"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("تاریخ انتشار")]
        public string PersianPublishDate { get; set; }


        [Display(Name = "زمان انتشار"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonIgnore]
        public string PersianPublishTime { get; set; }


        [JsonIgnore]
        public int UserId { get; set; }


        [JsonPropertyName("نویسنده")]
        public string AuthorName { get; set; }


        [JsonIgnore]
        public string ImageName { get; set; }


        [Display(Name = "تصویر شاخص"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore]
        public string ImageFile { get; set; }


        [JsonIgnore]
        public bool IsPublish { get; set; }


        [Display(Name = "نوع خبر"), Required(ErrorMessage = "انتخاب {0} الزامی است."), JsonIgnore()]
        public bool? IsInternal { get; set; }


        [JsonPropertyName("تگ ها")]
        public string NameOfTags { get; set; }


        [JsonPropertyName("نوع خبر")]
        public string NewsType { get; set; }


        [JsonPropertyName("بازدید")]
        public int NumberOfVisit { get; set; }


        [JsonPropertyName("NumberOfLike")]
        public int NumberOfLike { get; set; }


        [JsonPropertyName("NumberOfDisLike")]
        public int NumberOfDisLike { get; set; }


        [JsonPropertyName("NumberOfComments")]
        public int NumberOfComments { get; set; }


        [JsonPropertyName("دسته")]
        public string NameOfCategories { get; set; }


        [Display(Name = "آدرس خبر"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("آدرس"), UrlValidate("/", @"\", " ")]
        public string Url { get; set; }


        [JsonPropertyName("Status")]
        public string Status { get; set; }


        [JsonPropertyName("متن خبر")]
        public string Description { get; set; }


        [JsonIgnore]
        public bool IsBookmarked { get; set; }


        [JsonIgnore]
        public string[] CategoryIds { get; set; }


        [JsonIgnore]
        public string IdOfTags { get; set; }


        [JsonIgnore]
        public string IdOfCategories { get; set; }


        [JsonIgnore]
        public List<string> TagIdsList { get; set; }


        [JsonIgnore]
        public List<string> TagNamesList { get; set; }


        [JsonIgnore]
        public User AuthorInfo { get; set; }


        [JsonIgnore]
        public NewsCategoriesViewModel NewsCategoriesViewModel { get; set; }


        [JsonIgnore]
        public virtual ICollection<NewsCategory> NewsCategories { get; set; }


        [JsonIgnore]
        public virtual ICollection<NewsTag> NewsTags { get; set; }
    }
}
