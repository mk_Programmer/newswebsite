﻿using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace NewsWebsite.ViewModels.Video
{
    public class VideoViewModel
    {
        [JsonPropertyName("Id")]
        public string VideoId { get; set; }


        [JsonPropertyName("ردیف")]
        public int Row { get; set; }


        [Display(Name = "عنوان ویدیو"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), JsonPropertyName("عنوان ویدیو")]
        public string Title { get; set; }


        [Display(Name = "آدرس ویدیو"), Required(ErrorMessage = "وارد نمودن {0} الزامی است."), Url(ErrorMessage = "آدرس وارد شده نا معتبر است.")]
        public string Url { get; set; }


        [Display(Name = "پوستر ویدیو"), JsonIgnore]
        public IFormFile PosterFile { get; set; }


        public string Poster { get; set; }


        [JsonIgnore]
        public DateTime? PublishDateTime { get; set; }


        [JsonPropertyName("تاریخ انتشار")]
        public string PersianPublishDateTime { get; set; }
    }
}
