﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NewsWebsite.ViewModels.Home
{
    public class CategoryOrTagInfoViewModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public bool IsCategory { get; set; } // می شود false می شود و در غیر اینصورت true اگر اطلاعات دسته بندی درون پراپرتی های این کلاس قرار بگیرد, پراپرتی روبرو
    }
}
