﻿using System.ComponentModel.DataAnnotations;

namespace NewsWebsite.ViewModels.Api.UsersApi
{
    public class SignInBaseViewModel
    {
        [Display(Name = "نام کاربری"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string UserName { get; set; }


        [DataType(DataType.Password), Display(Name = "کلمه عبور"), Required(ErrorMessage = "وارد نمودن {0} الزامی است.")]
        public string Password { get; set; }
    }
}
