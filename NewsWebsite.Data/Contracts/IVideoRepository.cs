﻿using NewsWebsite.ViewModels.Video;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsWebsite.Data.Contracts
{
    public interface IVideoRepository
    {
        Task<List<VideoViewModel>> GetPaginateVideosAsync(int offset, int limit, string orderBy, string searchText);
        string CheckVideoFileName(string fileName);
    }
}
