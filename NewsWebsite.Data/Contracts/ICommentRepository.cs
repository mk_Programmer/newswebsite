﻿using NewsWebsite.ViewModels.Comments;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsWebsite.Data.Contracts
{
    public interface ICommentRepository
    {
        int CountUnAnsweredComments();
        Task<List<CommentViewModel>> GetPaginateCommentsAsync(int offset, int limit, string orderBy, string searchText, string newsId, bool? isConfirm);
    }
}
