﻿using NewsWebsite.Entities;
using NewsWebsite.ViewModels.Category;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NewsWebsite.Data.Contracts
{
    public interface ICategoryRepository
    {
        Task<List<CategoryViewModel>> GetPaginateCategoriesAsync(int offset, int limit, string orderBy, string searchText);
        Task<List<TreeViewCategory>> GetAllCategoriesAsync();
        Category FindByCategoryName(string categoryName);
        bool IsExistCategory(string categoryName, string recentCategoryId = null);
    }
}
