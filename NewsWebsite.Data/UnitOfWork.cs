﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using NewsWebsite.Data.Contracts;
using NewsWebsite.Data.Repositories;
using System.Threading.Tasks;

namespace NewsWebsite.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        public NewsDBContext Context { get; }
        private IMapper _mapper;
        private readonly IConfiguration _configuration;
        private ICategoryRepository _categoryRepository;
        private ICommentRepository _commentRepository;
        private INewsRepository _newsRepository;
        private INewsletterRepository _newsletterRepository;
        private ITagRepository _tagRepository;
        private IVideoRepository _videoRepository;
        public UnitOfWork(NewsDBContext context, IMapper mapper, IConfiguration configuration)
        {
            Context = context;
            _mapper = mapper;
            _configuration = configuration;
        }


        public IBaseRepository<TEntity> BaseRepository<TEntity>() where TEntity : class
        {
            // از طریق پولی مورفیسم ارسال می کنیم BaseRepository را به سازنده کلاس NewsDBContext نمونه ای از کلاس
            IBaseRepository<TEntity> repository = new BaseRepository<TEntity, NewsDBContext>(Context);
            return repository;
        }


        public ICategoryRepository CategoryRepository
        {
            get
            {
                if (_categoryRepository == null)
                    _categoryRepository = new CategoryRepository(Context, _mapper);

                return _categoryRepository;
            }
        }


        public ICommentRepository CommentRepository
        {
            get
            {
                if (_commentRepository == null)
                    _commentRepository = new CommentRepository(Context);

                return _commentRepository;
            }
        }


        public INewsRepository NewsRepository
        {
            get
            {
                if (_newsRepository == null)
                    _newsRepository = new NewsRepository(Context, _mapper, _configuration, this);

                return _newsRepository;
            }
        }


        public INewsletterRepository NewsletterRepository
        {
            get
            {
                if (_newsletterRepository == null)
                    _newsletterRepository = new NewsletterRepository(Context);

                return _newsletterRepository;
            }
        }


        public ITagRepository TagRepository
        {
            get
            {
                if (_tagRepository == null)
                    _tagRepository = new TagRepository(Context);

                return _tagRepository;
            }
        }


        public IVideoRepository VideoRepository
        {
            get
            {
                if (_videoRepository == null)
                    _videoRepository = new VideoRepository(Context);

                return _videoRepository;
            }
        }


        public async Task Commit()
        {
            await Context.SaveChangesAsync();
        }
    }
}
