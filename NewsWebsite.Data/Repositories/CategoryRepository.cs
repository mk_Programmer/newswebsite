﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NewsWebsite.Data.Contracts;
using NewsWebsite.Entities;
using NewsWebsite.ViewModels.Category;
using NewsWebsite.Common.Extensions;
using AutoMapper;
using System.Linq.Dynamic.Core;

namespace NewsWebsite.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly NewsDBContext _context;
        private readonly IMapper _mapper;
        public CategoryRepository(NewsDBContext context, IMapper mapper)
        {
            _context = context;
            _context.CheckArgumentIsNull(nameof(_context));

            _mapper = mapper;
            _mapper.CheckArgumentIsNull(nameof(_mapper));
        }


        public async Task<List<CategoryViewModel>> GetPaginateCategoriesAsync(int offset, int limit, string orderBy, string searchText)
        {
            // استفاده شده, زیرا می خواهیم اطلاعات دسته پدر را هم لود کنیم EagerLoading در این کوئری از ایگر لودینگ
            List<CategoryViewModel> categories = await _context.Categories.GroupJoin(_context.Categories, // Join and Group Join By 'LambdaExpression'
                    (pc => pc.ParentCategoryId),
                    (c => c.CategoryId),
                    ((pc, c) => new { CategoryInfo = pc, ParentInfo = c }))
                .SelectMany(p => p.ParentInfo.DefaultIfEmpty(), (x, y) => new { x.CategoryInfo, ParentInfo = y }) // Left Join
                .OrderBy(orderBy)
                .Skip(offset).Take(limit) // OrderByDescending() = مرتب سازی نزولی , Skip(offset).Take(limit) = لیمیت بگیر Take(limit) به اندازه آفست بپر و به اندازه  Skip(offset) , ToListAsync() = گرفتن اطلاعات در غالب یک لیست
                .Select(c => new CategoryViewModel
                {
                    CategoryId = c.CategoryInfo.CategoryId,
                    CategoryName = c.CategoryInfo.CategoryName,
                    ParentCategoryId = c.ParentInfo.CategoryId,
                    ParentCategoryName = c.ParentInfo.CategoryName
                }).AsNoTracking().ToListAsync();

            /* که شماره ردیف است برای اینکه دسته ها شماره ردیف داشته باشند و جهت نمایش شماره ها مقدار دهی شده Row در اینجا پراپرتی 
               ++offset = و ثانیا اگر در صفحه های بعد هستیم به ازای هر دسته بندی که در صفحه های بعد هست یک واحد اضافه می کنیم تا شماره ردیف های صفحات دیگر هم بدست می آید , تا اولا اگر در صفحه اول هستیم شماره ردسف از 1 شروع شود نه از صفر */
            foreach (var item in categories)
                item.Row = ++offset;

            return categories;
        }


        public async Task<List<TreeViewCategory>> GetAllCategoriesAsync()
        {
            var Categories = await (from c in _context.Categories
                                    where (c.ParentCategoryId == null)
                                    select new TreeViewCategory
                                    {
                                        id = c.CategoryId,
                                        title = c.CategoryName,
                                        url = c.Url
                                    }).ToListAsync();

            foreach (var item in Categories)
                BindSubCategories(item);

            return Categories;
        }


        public Category FindByCategoryName(string categoryName)
        {
            return _context.Categories.Where(c => c.CategoryName == categoryName.TrimStart().TrimEnd()).FirstOrDefault();
        }


        public bool IsExistCategory(string categoryName, string recentCategoryId = null)
        {
            if (!recentCategoryId.HasValue())
                return _context.Categories.Any(c => c.CategoryName.Trim().Replace(" ", "") == categoryName.Trim().Replace(" ", "")); // حذف فضای خالی دوطرف و نیز حذف فاصله بین کلمات
            else
            {
                var category = _context.Categories.Where(c => c.CategoryName.Trim().Replace(" ", "") == categoryName.Trim().Replace(" ", "")).FirstOrDefault(); // می شود IQueryable نباشد, متغیر از نوع FirstOrDefault() اینجا اگر متد

                if (category == null)
                    return false; // دسته ای همنام کتگوری نداریم و باید فالس را برگردانیم تا عمل ویرایش صورت گیرد
                else
                {
                    if (category.CategoryId != recentCategoryId)
                        return true; // دسته تکراری است و نمی توان آنرا ویرایش کرد
                    else
                        return false; // درج و ویرایش قابل انجام است چون دسته تکراری نیست 
                }
            }
        }


        public void BindSubCategories(TreeViewCategory category)
        {
            var SubCategories = (from c in _context.Categories
                                 where (c.ParentCategoryId == category.id)
                                 select new TreeViewCategory
                                 {
                                     id = c.CategoryId,
                                     title = c.CategoryName,
                                     url = c.Url
                                 }).ToList();

            foreach (var item in SubCategories)
            {
                BindSubCategories(item);
                category.subs.Add(item);
            }
        }
    }
}
